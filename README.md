# multiple-codequality-reports

## Test-Case for uploading multiple codequality reports

The upload of multiple codequality reports with wildcards fails with the error
`only one file can be sent as raw`

```
codequality:
    stage: test
    script:
        - 'true'
    artifacts:
        reports:
            codequality:
                - 'codequality.*.json'
```
